package com.safebear.auto.Pages;

import com.safebear.auto.Locators.ToolsPageLocator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class ToolsPage {

    ToolsPageLocator toolsPageLocator = new ToolsPageLocator();
    @NonNull
    WebDriver driver;

    public String getTitle () {
        return driver.getTitle();
    }
    public String checkForLoginSuccessfulMessage (){
        return driver.findElement(toolsPageLocator.getLoginSuccessfulMessage()).getText();
    }
//    entering test into search field
    public void enterTextSearchField(String toolName){
         driver.findElement(toolsPageLocator.getSearchField()).sendKeys(toolName);
    }
//    clicking button
    public void clickOnSearchButton(){
        driver.findElement(toolsPageLocator.getSearchButton()).click();
    }
//    check result
    public boolean checkForSeleniumTool (){
        return driver.findElement(toolsPageLocator.getSeleniumTool()).isDisplayed();
    }

}
