package com.safebear.auto.Pages;

import com.safebear.auto.Locators.LoginPageLocator;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;


@RequiredArgsConstructor
public class LoginPage {
    LoginPageLocator loginPageLocator = new LoginPageLocator();
    @NonNull
    WebDriver driver;

    public String getPageTitle (){
        return driver.getTitle();
    }
    public void enterUsername (String username){
        driver.findElement(loginPageLocator.getUsernameLocator()).sendKeys(username);
    }
    public void enterPassword (String password){
        driver.findElement(loginPageLocator.getPasswordLocator()).sendKeys(password);
    }
    public void clickLoginButton (){
        driver.findElement(loginPageLocator.getLoginButtonLocator()).click();
    }
    public String checkForFailedLoginMessage(){
        return driver.findElement(loginPageLocator.getFailedLoginMessage()).getText();
    }
}
