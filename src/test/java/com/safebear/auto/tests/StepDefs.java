package com.safebear.auto.tests;

import com.safebear.auto.Pages.LoginPage;
import com.safebear.auto.Pages.ToolsPage;
import com.safebear.auto.utils.Utils;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import gherkin.lexer.Sr_cyrl;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class StepDefs {
    //linking stepdefs to pages
    WebDriver driver;
    LoginPage loginPage;
    ToolsPage toolsPage;

    @Before
    public void setUp() {
        driver = Utils.getDriver();
//        linking objects
        loginPage = new LoginPage(driver);
        toolsPage = new ToolsPage(driver);
        driver.get(Utils.getUrl());
    }

    @After
    public void tearDown() {
        try {
            Thread.sleep(Integer.parseInt(System.getProperty("SLEEP", "2000")));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();
    }

    @Given("^I navigate to the login page$")
    public void i_navigate_to_the_login_page() throws Throwable {
//         Write code here that turns the phrase above into concrete actions
        Assert.assertEquals("Login Page", loginPage.getPageTitle(), "We're not on the Login Page or its title has changed");
    }

    @When("^I enter the login details for a '(.+)'$")
    public void i_enter_the_login_details_for_a_USER(String user) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        switch (user) {
            case "invalidUser":
                loginPage.enterUsername("attacker");
                loginPage.enterPassword("nothing");
                loginPage.clickLoginButton();
                break;

            case "validUser":
                loginPage.enterUsername("tester");
                loginPage.enterPassword("letmein");
                loginPage.clickLoginButton();
                break;

            default:
                Assert.fail("The test data is wrong - the only value that can be accepted are 'validUser' or 'invalidUser'");
                break;
        }


    }

    @Then("^I can see the following message: '(.+)'$")
    public void i_can_see_the_following_MESSAGE(String message) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        switch (message) {
            case "Username or Password is incorrect":
                loginPage.checkForFailedLoginMessage().contains(message);
                break;

            case "Login Successful":

                toolsPage.checkForLoginSuccessfulMessage().contains(message);
                break;

            default:
                Assert.fail("The test data is wrong");
                break;
        }
    }
    @Given("^that i am logged in$")
    public void that_i_am_logged_in() throws Throwable {
        loginPage.enterUsername("tester");
        loginPage.enterPassword("letmein");
        loginPage.clickLoginButton();
        Assert.assertEquals("Tools Page", toolsPage.getTitle());
    }

    @Given("^the (.+) tool exists$")
    public void the_tool_exists(String toolName) throws Throwable {
        Assert.assertTrue(toolsPage.checkForSeleniumTool());
    }

    @When("^I search for the tool (.+)")
    public void i_search_for_the_tool(String toolName) throws Throwable {
        toolsPage.enterTextSearchField(toolName);
        toolsPage.clickOnSearchButton();
    }

    @Then("^the tool (.+) is returned$")
    public void the_tool_is_returned(String toolName) throws Throwable {
        Assert.assertTrue(toolsPage.checkForSeleniumTool());
    }
}



