package com.safebear.auto.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.net.URL;

public class Utils {

    private static final String URL = System.getProperty("url","http://toolslist.safebear.co.uk:8080/");
    private static final String BROWSER = System.getProperty("browser", "chrome-largewindow");

    public static String getUrl (){return URL;}

    public static WebDriver getDriver () {

        System.setProperty("webdriver.chrome.driver","src/test/resource/driver/chromedriver.exe");

        //        adding firefox browser location
        System.setProperty("webdriver.gecko.driver","src/test/resource/driver/geckodriver.exe");

        ChromeOptions options = new ChromeOptions ();

        switch (BROWSER){
            case "chrome-largewindow":
                options.addArguments("window-size=1366,768");
                return new ChromeDriver(options);

            case "chrome-smallwindow":
                options.addArguments("window-size=66,8");
                return new ChromeDriver(options);

            case "headless":
                options.addArguments("headless","disable-gpu");
                return new ChromeDriver(options);

                default:
                    return new ChromeDriver();

            case "firefox":
                return new FirefoxDriver ();
        }



    }




}
