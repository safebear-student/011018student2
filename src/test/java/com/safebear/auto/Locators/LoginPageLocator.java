package com.safebear.auto.Locators;

import lombok.Data;
import org.openqa.selenium.By;
//lombok dependecny is added
@Data
//adding login page methods by using DOM identifiers on the page
public class LoginPageLocator {
    private By usernameLocator = By.id("username");
    private By passwordLocator = By.name("psw");
    private By loginButtonLocator = By.id("enter");
    private By failedLoginMessage = By.xpath("//p[@id='rejectLogin']/b");
}
