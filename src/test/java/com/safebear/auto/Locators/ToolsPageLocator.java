package com.safebear.auto.Locators;

import lombok.Data;
import org.openqa.selenium.By;
@Data
public class ToolsPageLocator {
    private By loginSuccessfulMessage = By.xpath(".//*[contains (text (),'Login Successful')]");
    private By searchField = By.id("toolName");
    private By searchButton = By.xpath("//input[contains(@placeholder,'Type')]");
    private By seleniumTool = By.xpath("//td[.=\'Selenium\']");

}
