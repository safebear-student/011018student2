Feature: Search

  Rules:
  * When you search for tool that exists then it is the only one which is returned
  * You must be logged in in order to search for a tool

  Background: Ensure that I am logged in
    Given that i am logged in

  @new
  Scenario: A user searches for a tool
    Given the selenium tool exists
    When I search for the tool selenium
    Then the tool selenium is returned

